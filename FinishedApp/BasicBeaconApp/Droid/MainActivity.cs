﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android;
using Xamarin.Forms;
using BLEBeaconFramework.Interfaces;
using BLEBeaconFramework.Droid;

namespace BasicBeaconApp.Droid
{
    [Activity(Label = "BasicBeaconApp.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
		static readonly int REQUEST_ACCESS_COURSE_LOCATION = 1;
		protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
			DependencyService.Register<IBeaconService, BeaconService>();

			global::Xamarin.Forms.Forms.Init(this, bundle);

			LoadApplication(new App());
        }
    }
}
