﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BLEBeaconFramework.Interfaces;
using Xamarin.Forms;

namespace BasicBeaconApp
{
    public class ItemsViewModel : BaseViewModel
    {
        public ObservableRangeCollection<IBeacon> Items { get; set; }
        public Command LoadItemsCommand { get; set; }
		public IBeaconService BeaconManager;

		public ItemsViewModel()
        {
            Title = "Beacons";
            Items = new ObservableRangeCollection<IBeacon>();

			BeaconManager = DependencyService.Get<IBeaconService>();

			BeaconManager.BeaconsSited += (sender, e) =>
			{
                // Clear and re-add each item (see Hack notes in the Readme to improve this)

                Items.Clear();
                foreach (var beacon in e.BeaconSitings.OrderByDescending(x => x.Minor).ToList())
                    Items.Add(beacon);
			};
		}

    }
}
