﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLEBeaconFramework.Interfaces;
using Xamarin.Forms;

namespace BasicBeaconApp
{
    public class ItemDetailViewModel : BaseViewModel
    {
        public IBeacon Item { get; set; }
        public ItemDetailViewModel(IBeacon item = null)
        {
            Title = item.Minor.ToString();
            Item = item;

        }

        int quantity = 1;
        public int Quantity
        {
            get { return quantity; }
            set { SetProperty(ref quantity, value); }
        }
    }
}
